// Practice with getting the information from objects, processing it, store, etc.

// Завдання 1
// Дано: функція яка приймає масив чисел або стрічок.
// Результат: вивести у консоль масив унікальних значень початкового масиву
// Приклад: [2, 3, 1, 3, 3, 7] => [2,3,1,7]

let arrOfNumbers = [2, 3, 1, 3, 3, 7];

function unicFn(initialArray) {
    let newArr = initialArray.filter( ( element, index, array ) => {
      return array.indexOf(element) === index; //console.log(`${element}:${index}:${array.indexOf(element)}`);
    });
    return newArr;
}

console.log(unicFn(arrOfNumbers));

// Завдання 2
// Дано: функція яка приймає масив чисел
// Результат: вивести у консоль "YES" якщо усі числа у масив парні та "NO" в іншому випадку
// Приклад:
// [1, 2, 3, 9] => “NO”
// [2, 4, 6] => “YES”
//DONE!!!!!!!!!!
function isEvenArray(initialArray) {
 // Write code here
   if (initialArray.every((i) => i%2 === 0)) {
   console.log("Yes");
   }
   else console.log("No");
}
isEvenArray([1, 2, 3, 9]);
isEvenArray([2, 4, 6]);

// Завдання 3
// Дано: функція яка приймає масив елементів будь-яких типів
// Результат: вивести у консоль масив який містить лише стрічки початкового масиву
// Приклад:
// [2, “string”, 3, , , ”test”] => [“string”, “test”]
//DONE!!!!!!!!!!
function filterArray(initialArray) {
 const commonArray = initialArray.filter((str) => {
   return typeof str == 'string';
 });
 console.log(commonArray);
}

filterArray([2, "string", 3, , , "test", 'string']);

// Завдання 4
// Дано: Функція приймає Об’єкт типу {[name]: {age: number, city: string}}
// Результат: Вивести у консоль масив із іменами людей які із міста "London" та старше 18 років
// Приклад:
// {Max: {age: 23, city: “London”}, Mike: {age: 20, city: “NY”}} => [“Max”]

//key[1] = Max: {age: 23, city: “London”} , key[2] = Mike: {age: 20, city: “NY”}
//DONE!!!!!!!!!!
function findUser(initialObject) {
 // Write code here
 const entries = Object.entries(initialObject);
 const namesArray = [];
 entries.forEach(([key, value]) => {
   if (value.age>18&&value.city=="London"){
   namesArray.push(key);
   }
 });
 console.log(namesArray);
}

findUser({Max: {age: 23, city: "London"}, Mike: {age: 20, city: "NY"},Mia: {age: 23, city: "London"}});

// Завдання 5 //DONE!!!
// Дано: Функція приймає три параметри: масив об`єктів [{}, {}], назву поля об`єкту (string), значення (string)
// Результат: Вивести у консоль новий масив з якого видалені усі об`єкти в яких keyName буде дорівнювати value
// Приклад:
// removeObj([{age: 1}, {age: 2}, {age: 2}, {year: 2}], "age", 2) => [ { age: 1 }, { year: 2 } ]

function removeObj(arrayOfObj, keyName, value) {
 const showKeyValue = arrayOfObj.filter(element => {
     if (!(Object.keys(element) == keyName && Object.values(element) == value)) {
         return true;
     }
 });
 console.log(showKeyValue);
}

const arrName = [{ age: 1 }, { age: 2 }, { age: 2 }, { year: 1 }];
removeObj(arrName, "age", 1);
removeObj(arrName, "year", 1);